package com.caspro.caspro_aibotts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;

import android.view.View;
import android.content.Intent;

import android.widget.Toast;

//import com.example.caspro_aibotts.R;

import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.RECORD_AUDIO;

public class MainActivity extends AppCompatActivity {

    int tc_accepted=0; //get num of rows
    public database mydb = new database(this);
    Button next_button;
    private final int MY_PERMISSIONS_RECORD_AUDIO = 1;
    private final int MY_PERMISSIONS_INTERNET = 2;


    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.t_and_c);
        start_check();

        // if start check returns 0 then wait-insert else move to next screen



        //add some data to question
        //-------------------------------------------------------------------------------------------
        mydb.insert_question("hello","question","","Hi");
        mydb.insert_question("how are you","question","","I am fine thank you");
        mydb.insert_question("what are you","question","","I am ai based app which can be programmed by user for social use.");



        //-------------------------------------------------------------------------------------------
        //


        if (!checkPermission()) {

            Toast.makeText(getApplicationContext(), "Need permissions", Toast.LENGTH_LONG).show();

            askPermission();

        }else {


            if (tc_accepted > 0) {
                Intent intent2 = new Intent(this, ai.class);

                context.startActivity(intent2);
                ((Activity)(this)).finish();


            }
        }



        next_button = (Button)findViewById(R.id.button2);
        // next button click listener
        next_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Boolean entry;

                    entry=mydb.insert_startup("Uk","yes");
                    Intent intent  = new Intent(MainActivity.this, ai.class);


                    context.startActivity(intent);
                    finish();
                }
                //((Activity)getContext()).finishActivity(1);


        });


    }

    public void start_check(){

        //try{
            tc_accepted=mydb.check_startup();
        //} catch (Exception e){

          //  tc_accepted=0;
        //}

    }



    public boolean checkPermission() {
        try{
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), INTERNET);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        //return result1 == PackageManager.PERMISSION_GRANTED;
        } catch (Exception e){
            return false;
        }
    }


    public void askPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECORD_AUDIO},MY_PERMISSIONS_RECORD_AUDIO);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.INTERNET},MY_PERMISSIONS_INTERNET);

    }






}
