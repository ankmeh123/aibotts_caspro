package com.caspro.caspro_aibotts;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

import android.view.View;
import android.content.Intent;
import android.widget.TextView;

import android.widget.Toast;


import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.view.MotionEvent;

import android.widget.ImageButton;

//import com.example.caspro_aibotts.R;

import java.util.ArrayList;
import java.util.Locale;


public class ai extends AppCompatActivity {


    String input;
    String previous_input;
    String Output;
    TextToSpeech t1;
    final SpeechRecognizer mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
    Button input_button;
    TextView input_text;

    ImageButton menu_button;
    private Context context;

    Integer logicgate=10;

    public database mydb2 = new database(this);
    String dateday;
    String monthday;
    String yearday;
    String ename;
    int listen=0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);






        // implement microphone

        final Intent mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());

        //implement speech
        t1 = new TextToSpeech(this, new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int arg0) {
                if(arg0 == TextToSpeech.SUCCESS)
                {
                    t1.setLanguage(Locale.UK);
                }
            }
        });


        //listen microphone
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {
                //getting all the matches
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                //displaying the first match
                if (matches != null)
                    input=matches.get(0);
                    // more commands here after first input call process function/method
                    process_input(input);


                    //
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });


        //microphone button touch
        findViewById(R.id.button).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        //when the user removed the finger
                        mSpeechRecognizer.stopListening();
                        //mSpeechRecognizer.destroy();
                        //editText.setHint("You will see input here");



                        break;

                    case MotionEvent.ACTION_DOWN:
                        //finger is on the button
                        //input="";
                         mSpeechRecognizer.startListening(mSpeechRecognizerIntent);

                        break;
                }
                return false;
            }
        });


        //manual process button listener
        input_text=(TextView) findViewById(R.id.editText);
        input_button = (Button)findViewById(R.id.button4);

        // process button click listener
        input_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
               process_input(String.valueOf(input_text.getText()));
            }
        });

        // menu button click listener
        menu_button = (ImageButton) findViewById(R.id.button2);
        menu_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                Intent intent3;
                intent3 = new Intent(ai.this, view_modules.class);
                context.startActivity(intent3);
                //finish();
            }
        });



    }





    public void process_input(String input){

        String ans;







        // look to static commands and define their logic gates


        if (logicgate==500)//load ebay
        {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://www.ebay.co.uk/sch/i.html?_nkw=" + input));
            startActivity(intent);
            output("ok, please check your device");
            logicgate=499;
            input_text.setText(null);
        }

        if (logicgate==600)//load amazon
        {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://www.amazon.co.uk/s?k=" + input));
            startActivity(intent);
            output("ok, please check your device");
            logicgate=499;
            input_text.setText(null);
        }
        if (logicgate==700)//load youtube
        {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://www.youtube.com/results?search_query=" + input));
            startActivity(intent);
            output("ok, please check your device");
            logicgate=499;
            input_text.setText(null);
        }
        if (logicgate==800)//load google
        {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://www.google.co.uk/search?tbm=shop&q=" + input));
            startActivity(intent);
            output("ok, please check your device");
            logicgate=499;
            input_text.setText(null);
        }


        if (logicgate==903)//input event date
        {
            //Intent intent = new Intent(Intent.ACTION_VIEW);
            //intent.setData(Uri.parse("https://www.google.co.uk/search?tbm=shop&q=" + input));
            //startActivity(intent);
            //output("ok, please check your device");
            //logicgate=499;
            ename="";
            ename=input;

            Boolean a;
            a=mydb2.insert_event(dateday,monthday,yearday,ename);

            output("Thanks");


            logicgate=499;
            input_text.setText(null);
        }



        if (logicgate==902)//input event date
        {
            yearday="";
            yearday=input;

            output("ok, please tell me event details using text box.");


            logicgate=903;
            input_text.setText(null);
        }




        if (logicgate==901)//input event date
        {
            monthday="";
            monthday=input;

            logicgate=902;
            output("ok, please tell me which year in form of 4 numbers using text box.");


            input_text.setText(null);
        }



        if (logicgate==900)//input event date
        {

            dateday="";
            dateday=input;

            logicgate=901;
            output("ok, please tell me which month between 1 to 12 using text box");


            input_text.setText(null);
        }



        if (logicgate==950)//today's events
        {


        }




        //==============================================
        // =============================================
        if (input.contains("eBay")){
            logicgate=500;
            output("ok, please tell me what to search");
        }
        if (input.contains("Amazon")){
            logicgate=600;
            output("ok, please tell me what to search");
        }
        if (input.contains("YouTube")){
            logicgate=700;
            output("ok, please tell me what to search");
        }
        if (input.contains("Google")){
            logicgate=800;
            output("ok, please tell me what to search");
        }




        //================
        if (input.contains("new event")){
            logicgate=900;
            output("ok, please tell me which date between 1 to 31 using text box");
        }

        if (input.contains("open event")){


            //===========

            Intent intent3;
            intent3 = new Intent(ai.this, view_module_custom.class);
            intent3.putExtra("valp","Events");
            context.startActivity(intent3);

            output("ok, please check your device");

            //===========
            logicgate=499;
            input_text.setText(null);


        }





        //=================


        if (logicgate==10)
        {

            ans=mydb2.check_question(input);

            if(ans.equals("0")){
                logicgate=100; //add question to db
            }else{
                output(ans);
            }
        }


        //add question to db
        //--------------------------------------------------------

        if (logicgate==102)//add question to db
        {


            Boolean a;
            a=mydb2.insert_question(previous_input,"","",input);
            output("Thanks");


            logicgate=10;
            input_text.setText(null);
        }





        if (logicgate==101)//add question to db
        {


            if (input.contains("yes") || input.contains("YES") )  {
                //ask for answer

                output("Great, Please input the response for your question.");
                logicgate=102;

            } else {
                //close input and set logic gate to initial stage
                output("ok");
                logicgate=10;

            }


            //output("");
            input_text.setText(null);
        }







        if (logicgate==100)
        {
            previous_input="";
            previous_input=input;

            output("Sorry I don't have any information regarding your Input, Would you like to add one. Please say yes or no.");


            logicgate=101;
            input_text.setText(null);
        }







        ////===============================================================================
        if (logicgate==499)//add question to db
        {
            logicgate=10;
            input_text.setText(null);
        }







        input_text.setText(null);
    }










    public void output(String p_output){

        Output= p_output;


        t1.speak(Output,TextToSpeech.QUEUE_FLUSH,null);



        Toast.makeText(getApplicationContext(), Output,Toast.LENGTH_SHORT).show();


        input_text.setText(null);




    }






}
