package com.caspro.caspro_aibotts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import android.database.DatabaseUtils;

public class database extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "aibottsdb";
    private static final int DATABASE_VERSION = 1;



    public database(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    //this function called only once ever in the life of the app
    @Override
    public void onCreate(SQLiteDatabase database) {
        //Create database query
        database.execSQL("create table " + "startup" + " (id INTEGER PRIMARY KEY AUTOINCREMENT,country text, accept_tc text); ");

        database.execSQL("create table " + "question" + " (id INTEGER PRIMARY KEY AUTOINCREMENT,question text, type text,instructions text,response text); ");

        database.execSQL("create table " + "ereminders" + " (id INTEGER PRIMARY KEY AUTOINCREMENT,ename text, edate text); ");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //add more insert query if you need to add more datas after, but you have first to upgrade your DATABASE_VERSION to a higher number
        db.execSQL("DROP TABLE IF EXISTS " + "startup");
        db.execSQL("DROP TABLE IF EXISTS " + "question");
        db.execSQL("DROP TABLE IF EXISTS " + "ereminders");
        onCreate(db);
    }


    public boolean insert_startup (String country, String accept_tc) {
        try {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("country", country);
        contentValues.put("accept_tc", accept_tc);

        db.insert("startup", null, contentValues);
        return true;
        } catch (Exception e){
            return false;
        }

 }




    public boolean insert_question (String question, String type, String instructions, String response) {

        try {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("question", question);
        contentValues.put("type", type);
        contentValues.put("instructions", instructions);
        contentValues.put("response", response);



        //db.insert("question", null, contentValues);


        String querys= "SELECT * FROM question where question='" +  question  + "'";
        Cursor mCursor = db.rawQuery(querys,null);

        if(mCursor.getCount() > 0) {

        }else{
            db.insert("question", null, contentValues);
        }



        return true;
        } catch (Exception e){
            return false;
        }

    }


    public String check_question(String question){

        String msg="";
        SQLiteDatabase db = this.getReadableDatabase();

        try{
            //String querys= "SELECT * FROM question where question LIKE '" +  question  + "%'";


            String querys= "SELECT * FROM question where question LIKE '" +  question  + "'";

            Cursor mCursor = db.rawQuery(querys,null);

            if(mCursor.getCount() > 0) {
                // means search has returned data
                if (mCursor.moveToFirst())
                {
                    msg=mCursor.getString(mCursor.getColumnIndex("response"));
                    //msg="found it";

                }

            }
            else
            {
                msg="0";
            }




        }catch (Exception e)
        {
            msg="0";
        }





        return msg;
        //return mCursor.getString(mCursor.getColumnIndex("response"));
    }

    public int check_startup(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "startup");
        return numRows;
    }


    public Cursor loadtable(String tablename, int page){

        SQLiteDatabase db = this.getReadableDatabase();
        String querys= "SELECT question FROM question";


        if (tablename.contains("Events")){
           //
            querys= "SELECT ename,edate FROM ereminders";

        }else{


        }

        Cursor mCursor = db.rawQuery(querys,null);
        //00000000
        return mCursor;
    }


    public Boolean deleterecord(String tablename, String name){

        try{
        //000000000


            if (tablename.contains("Events")){
                //
                SQLiteDatabase db = this.getReadableDatabase();
                String querys= "Delete FROM ereminders where ename='" + name + "'";
                db.execSQL(querys);


            }else{

                SQLiteDatabase db = this.getReadableDatabase();
                String querys= "Delete FROM question where question='" + name + "'";
                //querys= "Delete FROM ereminders where name='" + name + "'";
                db.execSQL(querys);

            }


            return true;

        } catch (Exception e){
            return false;
        }
    }


    public boolean insert_event (String day, String month, String year, String name) {

        try{
        SQLiteDatabase db = this.getWritableDatabase();

        String ddate=day + "/" + month + "/" + year ;
        ContentValues contentValues = new ContentValues();
        contentValues.put("ename", name);
        contentValues.put("edate", ddate);




        //db.insert("question", null, contentValues);


        db.insert("ereminders", null, contentValues);




        return true;

        } catch (Exception e){
            return false;
        }

    }






}
