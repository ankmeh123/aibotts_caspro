package com.caspro.caspro_aibotts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//import com.example.caspro_aibotts.R;


public class view_module_custom extends  AppCompatActivity{

    ImageButton menu_button;
    String selectedtable;
    String selectedstring;
    private Context context;
    boolean choose=false;
    public database mydb2 = new database(this);






    @Override
    protected void onCreate(Bundle savedInstanceState) {

        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_custom_entry);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedtable = extras.getString("valp");
            //The key argument here must match that used in the other activity
            Toast.makeText(getApplicationContext(), selectedtable,Toast.LENGTH_SHORT).show();
        }




        // menu button click listener
        menu_button = (ImageButton) findViewById(R.id.button3);
        menu_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                //Intent intent3;
                //intent3 = new Intent(view_modules.this, ai.class);
                //context.startActivity(intent3);
                //Toast.makeText(getApplicationContext(), "finish",Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        //''''''''


        Cursor data;


        data=mydb2.loadtable(selectedtable,1);


        final ListView listView;
        //String[] listItem;
        //String[] listusers = { "Input Dictionary", "Events"};
        ArrayAdapter aAdapter;

        List<String> noteList = new ArrayList <String>();


        if (selectedtable.contains("Events")){

            //---------------------
            if (data != null ) {
                if  (data.moveToFirst()) {
                    do {
                        String quest = data.getString(data.getColumnIndex("ename"));
                        String qdate = data.getString(data.getColumnIndex("edate"));
                        noteList.add(quest);
                    }while (data.moveToNext());
                }
            }
            data.close();

            //----------------------




        }else{

            //---------------------
            if (data != null ) {
                if  (data.moveToFirst()) {
                    do {
                        String quest = data.getString(data.getColumnIndex("question"));
                        noteList.add(quest);
                    }while (data.moveToNext());
                }
            }
            data.close();

            //----------------------


        }






        listView=(ListView)findViewById(R.id.list1);
        //listItem = getResources().getStringArray(R.array.ai_technology);
        aAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, noteList);


        listView.setAdapter(aAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                final String valp = "" + listView.getItemAtPosition(position);
                //=========================================================================================
                selectedstring= "" + listView.getItemAtPosition(position);
                dialogbox();


                //=========================================================================================



            }
        });






        //'''''''



    }



    public void dialogbox(){

        choose=false;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete record?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here
                choose=true;
                if (choose == true) {

                    Toast.makeText(getApplicationContext(), selectedstring, Toast.LENGTH_SHORT).show();

                    boolean result=mydb2.deleterecord(selectedtable,selectedstring);



                    finish();
                }

                dialog.dismiss();
            }

        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();





        //return choose;
    }



}
