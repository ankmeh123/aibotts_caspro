package com.caspro.caspro_aibotts;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

//import com.example.caspro_aibotts.R;


public class view_modules extends AppCompatActivity {



    ImageButton menu_button;
    ListView listView;
    String[] listItem;
    private Context context;
    private String[] listusers = { "Input Dictionary", "Events"};
    private ArrayAdapter aAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.module_screen);




        // menu button click listener
        menu_button = (ImageButton) findViewById(R.id.button3);
        menu_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                //Intent intent3;
                //intent3 = new Intent(view_modules.this, ai.class);
                //context.startActivity(intent3);
                //Toast.makeText(getApplicationContext(), "finish",Toast.LENGTH_SHORT).show();
                finish();
            }
        });




        listView=(ListView)findViewById(R.id.list1);
        listItem = getResources().getStringArray(R.array.ai_technology);
        aAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listusers);
        listView.setAdapter(aAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String valp = "" + listView.getItemAtPosition(position);

                //Toast.makeText(getApplicationContext(), ""+ listView.getItemAtPosition(position), Toast.LENGTH_SHORT).show();


                Intent intent3;
                intent3 = new Intent(view_modules.this, view_module_custom.class);
                intent3.putExtra("valp",valp);
                context.startActivity(intent3);





            }
        });







    }

}
