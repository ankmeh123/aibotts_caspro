package com.example.caspro_aibotts;

import com.caspro.caspro_aibotts.MainActivity;
import com.caspro.caspro_aibotts.database;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void test1() {

        database testdatabase = new database(null);
        assertEquals(false, testdatabase.insert_startup("test","test"));

    }


    @Test
    public void test2() {

        database testdatabase = new database(null);
        assertEquals(false, testdatabase.insert_question("test","test","test","test"));

    }


    @Test
    public void test3() {

        database testdatabase = new database(null);
        assertEquals(false, testdatabase.deleterecord("test","test"));

    }

    @Test
    public void test4() {

        database testdatabase = new database(null);
        assertEquals(false, testdatabase.insert_event("test","test","test","test"));

    }


    @Test
    public void test5() {

        MainActivity testdatabase = new MainActivity();
        assertEquals(false, testdatabase.checkPermission());

    }





}